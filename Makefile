main: main.o
	ld.lld -T link.lds -o $@ $<

%.o: %.s
	clang -mno-relax --target=riscv32 -c -o $@ $^

clean:
	$(RM) main *.o

.PHONY: clean
